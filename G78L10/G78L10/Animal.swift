//
//  Animal.swift
//  G78L5
//
//  Created by Ivan Vasilevich on 11.03.2020.
//  Copyright © 2020 ivan.besarab. All rights reserved.
//

import UIKit

enum AnimalType {
	case alligator//: "Alligator"
	case crocodile
	case cat
}

class Owner {
	weak var pet: Animal?
	deinit {
		print("deinited: \(String(describing: Self.self))")
	}
}

class Animal: NSObject { // Refference type
	
	var owner: Owner?
	
	var name: String {
		return self.description
	}
	
	
	let type: AnimalType
	var weight: Int
	private var kills: Int
	
	func eat() {
		print("yummy!")
		kills += 1
		otrizka()
		weight += 10
	}
	
	deinit {
		print("deinited: \(String(describing: Self.self))")
	}

	private func otrizka() {
		print("ORRRGHHH!!!")
	}
	
	override var description: String {
		return "This is \(type) weight = \(weight)kg"
	}
	
	init(type: AnimalType) {
		self.type = type
		self.weight = 0
		self.kills = 0
	}

}

struct AnimalStruct { // Value type
	var type: AnimalType
	var weight: Int
}
