//
//  ViewController.swift
//  G78L10
//
//  Created by Ivan Vasilevich on 13.04.2020.
//  Copyright © 2020 ivan.besarab. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
	
	var someAnimal = Animal.init(type: .cat)
	var someAnimalStruct = AnimalStruct.init(type: .alligator, weight: 0)

	override func viewDidLoad() {
		super.viewDidLoad()
		
		let kRunCount = "kRunCount"
		
		
		let runCount = UserDefaults.standard.integer(forKey: kRunCount) + 1 // read
		
		UserDefaults.standard.setValue(runCount, forKey: kRunCount) // write
		
		print("App start #\(runCount)")
//		readDrinksFromPlist()
		
		let isDark = UIScreen.main.traitCollection.userInterfaceStyle == .dark
		
		if isDark {
			view.backgroundColor = .black
		} else {
			view.backgroundColor = .white
		}
		
		view.backgroundColor = isDark ? .black : .white
		
		let image = UIImage(named: "coffee_maker")!
		print(image.size)
		let imgView = UIImageView(image: image)
		view.addSubview(imgView)
		imgView.center = view.center
		
		imgView.image = image
		
	}
	
	private func readDrinksFromPlist() {
		let path = Bundle.main.path(forResource: "Drinks", ofType: "plist")!
		let dictionary = NSMutableDictionary(contentsOfFile: path)
		print(dictionary!)
		dictionary?.setValue("Hello my crocodile", forKey: "Animal")
		dictionary?.write(toFile: "/Users/ivanvasilevich/Developer/ios2402wa-g78/G78L10/G78L10/Drinks copy.plist", atomically: true)
	}
	
	private func playWithAnimal() { // Refference types
		print(someAnimal.name)
				let myAnimal1 = someAnimal
				myAnimal1.eat()
				
				print(someAnimal.name)
				someAnimal.eat()
				let brothersAnimal = myAnimal1
				brothersAnimal.eat()
				print(someAnimal.name)
		//		print("animal obj have \(CFGetRetainCount(brothersAnimal)) owner")
	}
	
	private func valueTypes() {
		// Do any additional setup after loading the view.
		someAnimalStruct.weight = 33
		print(someAnimalStruct.weight)
		var myAnimal2 = someAnimalStruct
		myAnimal2.weight = 44
		print(someAnimalStruct.weight)
	}


}

