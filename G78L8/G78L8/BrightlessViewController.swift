//
//  BrightlessViewController.swift
//  G78L8
//
//  Created by Ivan Vasilevich on 06.04.2020.
//  Copyright © 2020 ivan.besarab. All rights reserved.
//

import UIKit

class BrightlessViewController: UIViewController {
	
	@IBOutlet weak var valueLabel: UILabel!
	@IBOutlet weak var valueSlider: UISlider!
	weak var previousVC: ViewController!
	
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
		setupView()
    }
	
	private func setupView() {
		 valueSlider.value = previousVC.brightlessLevel
	}
	
	@IBAction func valueChanged(_ sender: UISlider) {
		previousVC.brightlessLevel = sender.value
		valueLabel.text = sender.value.description
	}
	
	@IBAction func valueChanged(_ sender: UISlider, forEvent event: UIEvent) {
		print(event)
		navigationController?.popViewController(animated: true)
	}
	
}
