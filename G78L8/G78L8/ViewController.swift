//
//  ViewController.swift
//  G78L8
//
//  Created by Ivan Vasilevich on 06.04.2020.
//  Copyright © 2020 ivan.besarab. All rights reserved.
//

import UIKit


class ViewController: UIViewController {
	
	@IBOutlet private weak var lightSwitch: UISwitch!
	@IBOutlet private weak var lightSwitch2: UISwitch?
	@IBOutlet private weak var lampView: UIView!
	
	var brightlessLevel: Float = 1
	
	override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
		super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
	}
	
	override func awakeFromNib() { //xib // nib
		super.awakeFromNib()
		log()
		print("-------------1-------------------")
	}

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		log()
		print("-------------2-------------------")
		
		print(lightSwitch.isOn)
		print(lightSwitch2!.isOn)
		print(lightSwitch2?.isOn ?? false)
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		log()
		updateView()
	
		navigationItem.title = "Hello"
	}
	
	override func viewWillLayoutSubviews() {
		super.viewWillLayoutSubviews()
		log()
	}
	
	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()
		log()
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		log()
//		tabBarItem.badgeValue = ""
		navigationController?.tabBarItem.badgeValue = nil
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		log()
	}
	
	override func viewDidDisappear(_ animated: Bool) {
		super.viewDidDisappear(animated)
		log()
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
		log()
	}
	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		log()
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		log()
		
		if let brightlessVC = segue.destination as? BrightlessViewController {
			brightlessVC.previousVC = self
		}
	}
	
//	override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
//		let internetIsOn = Bool.random()
//		if internetIsOn {
//			return true
//		}
//		return false
////		fatalError("fatallity")
//	}
	
	@IBAction private func lampStateSwitched(_ sender: UISwitch) {
		lampView.isHidden = !sender.isOn
	}
	
	private func updateView() {
		lampView.alpha = CGFloat(brightlessLevel)
	}
	
	deinit {
		log()
	}
}
