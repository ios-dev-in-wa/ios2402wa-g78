//
//  Logger.swift
//  G78L8
//
//  Created by Ivan Vasilevich on 06.04.2020.
//  Copyright © 2020 ivan.besarab. All rights reserved.
//

import Foundation

func log(_ functionName: String = #function, line: Int = #line, file: String = #file, message: String = "") {
	let dateFormatter = DateFormatter()
	dateFormatter.dateFormat = "HH:mm:ss"
	print("\(dateFormatter.string(from: Date())) l#:\(line) \(functionName) in \((file as NSString).lastPathComponent) " + message)
}
