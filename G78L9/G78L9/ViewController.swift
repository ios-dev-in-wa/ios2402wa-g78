//
//  ViewController.swift
//  G78L9
//
//  Created by Ivan Vasilevich on 08.04.2020.
//  Copyright © 2020 ivan.besarab. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
		navigationItem.title = "Main"
		let testVC = TestViewController()
		navigationController?.tabBarController?.viewControllers?.append(testVC)
	}
	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		let pinkStoryboard = UIStoryboard(name: "Pink", bundle: nil)
		let pinkVC = pinkStoryboard.instantiateViewController(identifier: "pinkVC")
//		navigationController?.pushViewController(pinkVC, animated: true)
		present(pinkVC, animated: true, completion: nil)
		performSegue(withIdentifier: "goToSettings", sender: nil)
	}


}

