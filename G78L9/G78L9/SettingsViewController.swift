//
//  SettingsViewController.swift
//  G78L9
//
//  Created by Ivan Vasilevich on 08.04.2020.
//  Copyright © 2020 ivan.besarab. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
		
		navigationItem.title = "Settings"

        // Do any additional setup after loading the view.
    }
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
		navigationController?.tabBarItem.badgeValue = nil
	}
	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//		navigationController?.popViewController(animated: true)
		tabBarController?.selectedIndex = 1
	}
    

}
