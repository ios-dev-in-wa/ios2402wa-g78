//
//  ViewController.swift
//  G78L2
//
//  Created by Ivan Vasilevich on 26.02.2020.
//  Copyright © 2020 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
		let pi = 3.14
		print("pi =", pi)
		print("pi = \(pi)")
		let accel = 9.8
		print("pi = \(pi), accel = \(accel)")
		var a = 5
		a = 6
		let sum = a + Int(pi)
		
		variables()
		random()
		// < > <= >= == !=
		if a < sum {
			let c = 33
			print("sum pi + a = \(sum)")
			print(c)
		}
		
		// && AND / || OR
		let myCashMoreThanHalf = pi < accel, wifesCashMoreThanHalf = a > sum
		if myCashMoreThanHalf || wifesCashMoreThanHalf {
			print("DEAL")
		}
//		print(c)
		
		driveRobot()
		print("3 + 2 = \(sumN(a: 3, b: 2))")
		for i in 0..<6 {
			print(i)
		}
	}

	func variables() {
		
		// + - / * % Binary / - + Unary
		// 4 + 5 (operand1) (operator) (operand2)
		let a = -5
		var age = Double(a)
		age = 21
		age = age + 1
		age += 1
		print("age", age)
//		pi = 3.19
	}
	
	func random() { // A
//		variables()
//		print("foo pi =", pi)
		let studentNumber = randomStudentNumber()
		print("throw candy to student #\(studentNumber)")
	}
	
	func randomStudentNumber() -> Int { // B
		var result: Int = 0
		result = Int.random(in: 1...5)
		return result
	}
	
	func driveRobot() { // A
		move()
		let steps = 3
		move(moveCount: steps)
		move(moveCount: 2, smileType: "------------")
	}
	
	func move() {
		print("move()")
	}
	
	func move(moveCount: Int) { // B
		for _ in 0..<moveCount {
			move()
		}
	}
	
	func move(moveCount: Int, smileType: String) { // C
		print(smileType)
		move(moveCount: moveCount)
		print(smileType)
	}
	
	func sumN(a: Int, b: Int) -> Int {
		var result: Int = 0
		result = a + b
		return result
	}

}

