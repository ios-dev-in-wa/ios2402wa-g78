//
//  ViewController.swift
//  G78L7
//
//  Created by Ivan Vasilevich on 01.04.2020.
//  Copyright © 2020 ivan.besarab. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
	
	@IBOutlet weak var studentNameLabel: UILabel!
	private var students: [String] = []
	private var currentStudentIndex: Int = 0

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
//		var optInt: Int?
//
//		optInt = 5
//
//		print(optInt ?? 0) // default value
		initData()
		addBox()
//		buttonPressed()
	}
	
	private func initData() {
		students = ["Dmitryi", "Natasha", "Arsalan",  "Andrii"]
//						0			1  		  2			 3
	}
	
	func addBox() {
		let box = UIView()
		box.frame.size.width = 64
		box.frame.size.height = 64
		box.frame.origin.x = 128
		box.frame.origin.y = 256
		box.backgroundColor = .brown
		box.backgroundColor = UIColor(rgb: 0x358f4d)
		box.alpha = 0.33
		view.addSubview(box)
	}

	@IBAction func buttonPressed() {
		view.backgroundColor = .orange
	}
	
	@IBAction func showStudent(_ sender: UIButton) {
		sender.backgroundColor = .random
		print("currentStudentIndex:\(currentStudentIndex) sender.tag: \(sender.tag)")
		currentStudentIndex += sender.tag
		if currentStudentIndex == students.count {
			currentStudentIndex = 0
		} else if currentStudentIndex < 0 {
			currentStudentIndex = students.count - 1
		}
		
		let currentStudent = students[currentStudentIndex]
		studentNameLabel.text = currentStudent
		studentNameLabel.backgroundColor = .random
	}
	
}

