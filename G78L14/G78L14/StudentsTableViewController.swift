//
//  StudentsTableViewController.swift
//  G78L14
//
//  Created by Ivan Vasilevich on 29.04.2020.
//  Copyright © 2020 ivan.besarab. All rights reserved.
//

import UIKit

class StudentsTableViewController: UITableViewController {
	
    override func viewDidLoad() {
        super.viewDidLoad()

	}
	
	override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		let mainLastName = IndexPath.init(row: 3, section: 0)
		let middleNameIndexPath = IndexPath.init(row: 4, section: 1)
		
		switch indexPath {
		case mainLastName:
			return 0
		case middleNameIndexPath:
			return 0
		default:
			return super.tableView(tableView, heightForRowAt: indexPath)
		}
	}
}
