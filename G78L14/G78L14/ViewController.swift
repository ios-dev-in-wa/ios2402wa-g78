//
//  ViewController.swift
//  G78L14
//
//  Created by Ivan Vasilevich on 29.04.2020.
//  Copyright © 2020 ivan.besarab. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	@IBOutlet weak var centerConstraint: NSLayoutConstraint!
	@IBOutlet weak var redView: UIView!
	
	var closure: () -> () = {
		print("Dyadko Maksim")
	}
	
	override func awakeFromNib() {
		super.awakeFromNib()
		
		print("ViewController inited")
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
		
//		let strogRef = self
//		weak var weakRef = strogRef
		
		closure = { [unowned self] () -> ()  in
			print(self.view.frame.size)
		}
		
		let result = [1, -1].sorted { (a, b) -> Bool in
			return a > b
		}
		print(result)
		
		closure()
//		let b = closure(7, 8)
		
		
	}
	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		centerConstraint.constant += 20

		
		UIView.animate(withDuration: 2) {
			self.view.layoutIfNeeded()
			self.redView.transform = .init(rotationAngle: .pi)
//			self.redView.center.y += 40
		}
		
		
	}
	
	deinit {
		print("ViewController deinited")
	}
	
}

