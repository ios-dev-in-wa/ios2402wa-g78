//
//  ViewController.swift
//  G78L15
//
//  Created by Ivan Vasilevich on 06.05.2020.
//  Copyright © 2020 ivan.besarab. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
	
	@IBOutlet weak var imageView: UIImageView!
	@IBOutlet weak var button: UIButton!

	override func viewDidLoad() {
		super.viewDidLoad()
		
		
		NotificationCenter.default.addObserver(self,
											   selector: #selector(imageDownloaded(notification:)),
											   name: imageDownloadedNotification,
											   object: ImageDownloader.shared)
	}
	
	@objc func imageDownloaded(notification: Notification) {
		let downloadedimage = notification.userInfo?["pic"] as? UIImage
		imageView.image = downloadedimage
		
		button.isEnabled = true
//		dismiss(animated: true, completion: nil)
	}
	
	@IBAction func downloadButtonPressed(_ sender: UIButton) {
		sender.isEnabled = false
		let link = "https://gitlab.com/ios-dev-in-wa/ios2402wa-g78/-/raw/master/G78L15/G78L15/Assets.xcassets/mazdaFront.imageset/mazdaFront.png?inline=false"
		ImageDownloader.shared.downloadImageFromLink(link: link)
		showAlert()
	}
	
	func showAlert() {
		let isIpad = UIDevice.current.userInterfaceIdiom == .pad
		let alert = UIAlertController(title: "What me do?", message: "My friend is birthday today!", preferredStyle: .actionSheet)
		alert.addAction(UIAlertAction(title: "My generale", style: .default, handler: { (_) in
			self.view.backgroundColor = .systemRed
		}))
		alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (_) in
			self.view.backgroundColor = .systemBlue
		}))
		if isIpad {
			alert.modalPresentationStyle = .popover
			alert.popoverPresentationController?.sourceView = button
		}
		present(alert, animated: true, completion: nil)
	}
	
	deinit {
		NotificationCenter.default.removeObserver(self)
	}
	
}

