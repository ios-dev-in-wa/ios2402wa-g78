//
//  ViewController.swift
//  G78L13
//
//  Created by Ivan Vasilevich on 27.04.2020.
//  Copyright © 2020 ivan.besarab. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	@IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var umageView: UIImageView!
	
	var objects: [UIImage] = [] {
		didSet {
			umageView.image = objects.last
			tableView.reloadData()
		}
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
		objects.append(UIImage.init(systemName: "trash")!)
		objects.append(UIImage.init(systemName: "pencil")!)
		objects.append(UIImage.init(systemName: "trash.slash.fill")!)
		
		tableView.dataSource = self
		tableView.delegate = self
	}
	
	
	@IBAction func imageTapped(_ sender: UITapGestureRecognizer) {
		
		openImagePicker()
	}
	
	func openImagePicker() {
		let picker = UIImagePickerController()
		picker.delegate = self
		
		present(picker, animated: true, completion: nil)
	}
	

}

extension ViewController: UITableViewDelegate {
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		let itemToDisplay = objects[indexPath.row]
		let imageDisplayVC = ImageDisplayViewController()
		imageDisplayVC.imageToDisplay = itemToDisplay
		present(imageDisplayVC, animated: true, completion: nil)
	}
}

extension ViewController: UITableViewDataSource {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return objects.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let itemToDisplay = objects[indexPath.row]
		let cell = tableView.dequeueReusableCell(withIdentifier: "ObjectTableViewCell", for: indexPath) as! ObjectTableViewCell
		cell.centerImageView.image = itemToDisplay
		return cell
	}
}

extension ViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
	func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
		picker.dismiss(animated: true, completion: nil)
		guard let pickedImage = info[.originalImage] as? UIImage else {
			return
		}
		objects.append(pickedImage)
	}
}

