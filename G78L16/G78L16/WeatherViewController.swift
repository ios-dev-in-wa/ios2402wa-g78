//
//  WeatherViewController.swift
//  G78L16
//
//  Created by Ivan Vasilevich on 13.05.2020.
//  Copyright © 2020 ivan.besarab. All rights reserved.
//

import UIKit

class WeatherViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
		guard let request = NetworkManager.generateWeatherRequest() else {
			return
		}
		URLSession.fetchData(type: Forecast.self, request: request) { (forecast, error) in
			print(forecast!.list.first!.main.temp, forecast!.list.first!.main.temp_max)
		}

    }
}
