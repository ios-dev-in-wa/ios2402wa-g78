//
//  ViewController.swift
//  G78L16
//
//  Created by Ivan Vasilevich on 13.05.2020.
//  Copyright © 2020 ivan.besarab. All rights reserved.
//

import UIKit
import SDWebImage
import AVFoundation

class ViewController: UIViewController {

	@IBOutlet weak var imageView: UIImageView!
	var player: AVAudioPlayer!

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
		kBgQ.async {
			ImageDownloader.shared.longLoop {
				self.view.backgroundColor = .systemBlue
			}
		}
	}

	@IBAction func makeRed() {
		view.backgroundColor = view.backgroundColor == .red ? .systemGreen : .red
		let link = "https://3dnews.ru/assets/external/illustrations/2020/05/13/1010861/535.jpg"
		imageView.sd_setImage(with: URL(string: link), completed: nil)
	}

	@IBAction func playMusic() {
		let link = Bundle.main.url(forResource: "bgMusic", withExtension: "mp3")!
		do {
			player = try AVAudioPlayer(contentsOf: link)
		} catch {
			print(error)
			return
		}
		player.play()
	}

}
