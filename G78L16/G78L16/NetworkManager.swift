//
//  NetworkManager.swift
//  G78L16
//
//  Created by Ivan Vasilevich on 13.05.2020.
//  Copyright © 2020 ivan.besarab. All rights reserved.
//

import Foundation

struct Main: Codable {
	let temp: Double
	// swiftlint:disable identifier_name
	let temp_max: Double
	// swiftlint:enable identifier_name
}

struct List: Codable {
	let main: Main
}

struct Forecast: Codable {
	let cnt: Int
	let list: [List]
}

class NetworkManager {
	static func generateWeatherRequest() -> URLRequest? {

        guard var URL = URL(string: "https://api.openweathermap.org/data/2.5/forecast") else {return nil}
        let URLParams = [
            "q": "KIEV,uA",
            "mode": "{}",
            "APPID": "5a1afe5c9f21ea1636c623fdb880e0bb"
        ]
        URL = URL.appendingQueryParameters(URLParams)
        var request = URLRequest(url: URL)
        request.httpMethod = "GET"

		return request
    }
}

protocol URLQueryParameterStringConvertible {
    var queryParameters: String {get}
}

extension Dictionary : URLQueryParameterStringConvertible {
    /**
     This computed property returns a query parameters string from the given NSDictionary. For
     example, if the input is @{@"day":@"Tuesday", @"month":@"January"}, the output
     string will be @"day=Tuesday&month=January".
     @return The computed parameters string.
    */
    var queryParameters: String {
        var parts: [String] = []
        for (key, value) in self {
            let part = String(format: "%@=%@",
                String(describing: key).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!,
                String(describing: value).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
            parts.append(part as String)
        }
        return parts.joined(separator: "&")
    }

}

extension URL {
    /**
     Creates a new URL by adding the given query parameters.
     @param parametersDictionary The query parameter dictionary to add.
     @return A new URL.
    */
    func appendingQueryParameters(_ parametersDictionary : Dictionary<String, String>) -> URL {
        let URLString : String = String(format: "%@?%@", self.absoluteString, parametersDictionary.queryParameters)
        return URL(string: URLString)!
    }
}


