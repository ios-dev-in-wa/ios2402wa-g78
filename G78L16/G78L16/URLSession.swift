import Foundation

extension URLSession {
	static func fetchData<D: Decodable>(type: D.Type, request: URLRequest, completion: ((D?, Error?) -> Void)?) {
		URLSession.shared.dataTask(with: request) { data, response, error in
			if let data = data {
				do {
					let object = try JSONDecoder().decode(type, from: data)
					DispatchQueue.main.async {
						completion?(object, nil)
					}
				} catch let error {
					print(error)
					DispatchQueue.main.async {
						completion?(nil, error)
					}
				}
			}
		}.resume()
	}
}
