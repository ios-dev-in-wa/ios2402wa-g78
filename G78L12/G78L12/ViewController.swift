//
//  ViewController.swift
//  G78L12
//
//  Created by Ivan Vasilevich on 22.04.2020.
//  Copyright © 2020 ivan.besarab. All rights reserved.
//

import UIKit
//IQKeyboardManager
enum Drink: Int {
	case cappucino, americano
}

class ViewController: UIViewController  {
	
	@IBOutlet weak var nameTextField: UITextField!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
		nameTextField.delegate = self
	}
	
	private func structTest() {
		print(Drink.americano.rawValue)
		
		if CGPoint.zero == .zero {
			print("CGPoint.zero == .zero")
		}
		
		let ing = CoffeeIngredients(a: 3)
		print(ing)
		var oldFrame = view.frame
		oldFrame.origin.x += 10
		oldFrame.origin.y += 20
		let newBox = UIView(frame: oldFrame)
		newBox.backgroundColor = .systemPink
		view.addSubview(newBox)
	}
	
	private func workWithPrototcols() {
		var repeatableObjects: [NameRepeater] = []
		repeatableObjects.append(Bear())
		repeatableObjects.append(Bear())
		repeatableObjects[0].repeatName(3)
	}
	
}

extension ViewController: UITextFieldDelegate {
	// MARK: - UITextFieldDelegate
	func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
		if textField == nameTextField {
			return nameTextField.text!.isEmpty
		}
		return false
	}
	
	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		textField.resignFirstResponder() // hide keyboard
		return true
	}
}







