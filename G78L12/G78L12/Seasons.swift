//
//  Seasons.swift
//  G78L12
//
//  Created by Ivan Vasilevich on 22.04.2020.
//  Copyright © 2020 ivan.besarab. All rights reserved.
//

import Foundation

@objc protocol Namable {
	var name: String {get}
}

protocol NameRepeater: Namable {
	func repeatName(_ count: Int)
}

@objc protocol Seasons: Namable {
	func winterCome()
	@objc optional func didEnterCharacter(_ str: String) -> [String]
}

protocol Sellable {
	var price: Double {get}
}

class Rabbit: Seasons {
	var name: String = "Rabbit"
	
	func winterCome() {
		print("self.shkura.color = .white")
	}
	
	func didEnterCharacter(_ str: String) -> [String] {
		let apps = ["111", "211", "333"]
		let sortedApps = apps.filter { (appName) -> Bool in
			appName.contains(str)
		}
		return sortedApps
	}
	
}

class Bear: Seasons, Sellable, NameRepeater {
	
	var name: String {
		return "Bear"
	}

	func repeatName(_ count: Int) {
		for _ in 0..<count {
			print(self.name)
		}
	}
	
	var price: Double = 100
	
	func winterCome() {
		print("self.suckHand()")
	}
	
}


//let namableObjects = [Bear(), Rabbit()] as [Seasons]
//namableObjects.first?.name.count
