//
//  ViewController.swift
//  G78L3
//
//  Created by Ivan Vasilevich on 02.03.2020.
//  Copyright © 2020 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
		//		abs(-99)
		move(steps: 5) //A
		//		move(5) //B
		move(a: 5)
//		let myCar = ""
//		paintCar(myCar, color: .red)
//		paintCar(myCar, with: .red)
//		workWithStrings()
		workWithArrays()
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(true)
	}
	
	func move(steps: Int) { // A
		print(steps)
	}
	
	func move(_ steps: Int) { // B
		print(steps)
	}
	
	func move(a b: Int) {
		print(b)
	}
	
	func paintCar(color: UIColor) {
		print(color)
	}
	
	func workWithStrings() {
//		let int0: Int
//		int0 = 55
//		print(int0)
//		let int1 = Int()
//		print(int1)
//		if Int() == 0 { //true
//		let str = String()
//		let str = ""
//		let myNewColor = UIColor()
		let myName = "Ivan"
		let secondName = "Bes"
		print("Hello, \(myName) " + secondName)
		print("\(myName) consist of \(myName.count) characters")
		let longString = """
//		let int0: Int
//		int0 = 55
//		print(int0)
//		let int1 = Int()
//		print(int1)
//		if Int() == 0 { //true
//		let str = String()
//		let str = ""
//		let myNewColor = UIColor()
"""
		print(longString.replacingOccurrences(of: "\t", with: ""))
	}
	
	func workWithArrays() {
		let longStr = "ya Hello, Ivan Bes"
		var array = longStr.components(separatedBy: " ")//Array<String>()
		let dict = [String : String]()
		let set = Set(array)
		print(array)
		print(set)
		print(array.count)
		array.append("ready")
		print(array[4])
	}
	
	
}

