//
//  TriangleView.swift
//  G78l11
//
//  Created by Ivan Vasilevich on 15.04.2020.
//  Copyright © 2020 ivan.besarab. All rights reserved.
//

import UIKit

class TriangleView: UIView {
	
	override func awakeFromNib() {
		super.awakeFromNib()
		
		setup()
	}
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		
		setup()
	}
	
	required init?(coder: NSCoder) {
		super.init(coder: coder)
	}
	
	private func setup() {
		print("TriangleView setup")
		backgroundColor = .white
		perform(#selector(recolor))
	}
	
	@objc func recolor() {
		
	}

    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
//		let image = UIImage(named: "triangle")!
//		image.draw(in: bounds)
		UIColor.red.setStroke()
		let linePath = UIBezierPath()
		linePath.lineWidth = 4
		
		
		let trianglePath = UIBezierPath()
		trianglePath.lineWidth = linePath.lineWidth
		trianglePath.move(to: CGPoint(x: (bounds.width / 100) * 50,
							  y: (bounds.height / 100) * 0))
		trianglePath.addLine(to: CGPoint(x: (bounds.width / 100) * 100,
								 y: (bounds.height / 100) * 100))
		trianglePath.addLine(to: CGPoint(x: (bounds.width / 100) * 0,
								 y: (bounds.height / 100) * 100))
		
		trianglePath.close()
		linePath.move(to: CGPoint(x: (bounds.width / 100) * 00,
								  y: (bounds.height / 100) * 70))
		linePath.addLine(to: CGPoint(x: (bounds.width / 100) * 100,
									 y: (bounds.height / 100) * 70))
		linePath.stroke()
		trianglePath.addClip()
		
		trianglePath.stroke()
		
		
		
    }

}
