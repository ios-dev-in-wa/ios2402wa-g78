//
//  ViewsDemoViewController.swift
//  G78l11
//
//  Created by Ivan Vasilevich on 15.04.2020.
//  Copyright © 2020 ivan.besarab. All rights reserved.
//

import UIKit

class ViewsDemoViewController: UIViewController {
	@IBOutlet weak var faceView: FaceView!
	@IBOutlet weak var triangleView: TriangleView!
	
	override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
		faceView.happyLevel = 40
    }
    
}
