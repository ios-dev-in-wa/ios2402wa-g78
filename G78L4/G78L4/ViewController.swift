//
//  ViewController.swift
//  G78L4
//
//  Created by Ivan Vasilevich on 04.03.2020.
//  Copyright © 2020 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
	
	enum Color: String, CaseIterable {
		case red = "R" // = 0
		case pink = "Pi"// = 1
		case purpure = "Pu"
	}

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
		print(workWithNSString())
		mutableImutableArray()
		switchExample()
		showColor(.red)
		optionalUnwraping()
		print("end")
	}

	func workWithNSString() -> String {
		let str = "Hello"
		let nsstr = str as NSString //NSString(str)
		let a = nsstr.appending(nsstr.description)
//		print(nsstr.substring(with: NSRange(location: 1, length: 3)))
		let str3 = nsstr as String //
//		print(UIColor.red)
//		print(UIColor.red.description)
//		print(str3)
		return str + nsstr.description + a + str3
	}
	
	func mutableImutableArray() {
		let arr = [1, 2]
		let nsMutableArray = NSMutableArray(array: arr)
		nsMutableArray.add("3")
		print(nsMutableArray)
		let element = nsMutableArray[1]
		print(element)
		// Downcast
		if let intElement = element as? Int {
			print(intElement)
		}
	}
	
	func switchExample() {
		let internetErrorr = Int.random(in: 0...2)
		
		switch internetErrorr {
		case 0, 100, 200:
			print("no connection")
			fallthrough
		case 1:
			print("poor connection")
		case 2:
			print("port is blocked")
		case let errorCode where errorCode >= 50 && errorCode <= 100:
			print("port is blocked")
		default:
			print("unknown error")
		}
	}
	
	func enumExample() {
		
		let randomColor = Color.allCases.randomElement()!
		
		switch randomColor {
		case .purpure:
			print("standart price")
		case .red:
			print("double price")
		case .pink:
			print("triple price")
		}
	}
	
	func showColor(_ color: Color) {
		if color == .red {
			view.backgroundColor = .cyan
		}
	}
	
	func optionalUnwraping() {
		let number = 5
		let strFromNumber = "5a"//number.description//"\(number)"//String(number)
		let numberFromString = Int(strFromNumber)
		
//		print(numberFromString + 3)
		// Optional binding
		if let realNumberFromString = numberFromString {
			print(realNumberFromString + number)
		} else {
			print("enter correct number")
		}
		
		// Guard
		guard let realNumberFromString = numberFromString else {
			print("enter correct number")
			return
		}
		print(realNumberFromString + number)
		
		// Default value
		print((numberFromString ?? 0) + number)
		
		// Force unwrap
		print(numberFromString! + number)
		
	}
	
	func workWithDictionaries() {
		let phoneBook: [String : String]  = [
			//key :		value
			"Ivan1": "911-002-003",
			"Ivan2": "911-002-003",
			"Ivan3": "911-002-003",
			"Ivan": "911-002-003",
			"Ivan": "922-002-003",
		]
		let myNumber = phoneBook["ivan"]
		print(myNumber ?? "")
	}

}

